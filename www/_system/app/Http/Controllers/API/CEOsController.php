<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CEOsController extends Controller
{
    public function setData(\Illuminate\Http\Request $request)
    {
        $log = new \App\Models\Log\LogAdsPolice();
        $log->action = '22ceos';
        $log->object = 'complete';
        $log->data = json_encode($request->all());
        $log->save();
        
        
        $CEOs = new \App\Models\CEOs($request->all());
        $CEOs->save();
    }
    public function test()
    {
        return view('test');
    }
    
    public static function upload(Request $request) {
        
        //$file = $request->file('cv');        
        //dd($file, $request->hasFile('cv'));
        
        //return response()->json('upload_file_not_found');
        
        //return (string) $request->hasFile('file');
        
        if(!$request->hasFile('cv')) {
            return response()->json('upload_file_not_found');
        }
        $file = $request->file('cv');
        if(!$file->isValid()) {
            return response()->json('invalid_file_upload');
        }
        
        $path = public_path() . '/uploads/';
        $filename = \Faker\Provider\Uuid::uuid().''.\Carbon\Carbon::now()->toDateString().'_'.\Carbon\Carbon::now()->toTimeString();//.'_'.$file->getClientOriginalName();
        
        $file->move($path, $filename );
        return response()->json('http://localhost/uploads/'.$filename);
    }
    
}

