<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Box;
use App\Models\Log\LogBox;

interface iSGLogAPIController {
    
    public static function create(Request $request);
    public static function get(Request $request);
    public static function getByBoxId(Request $request);
    
}

class SGLogAPIController extends Controller implements iSGLogAPIController
{
    
    public static function test() {
        return 'test';
    }
    
    public static function create(Request $request) {
        
        $log_box = new LogBox($request->toArray());
        $log_box->save();
        
        $response_data = self::getResponseData();
        $response_data->data = $log_box;
        
        return response()->json($response_data);
        
    }
    
     public static function get(Request $request) {
        
        $log_box = LogBox::where('id', $request->id)->first();
        $response_data = self::getResponseData();
        $response_data->data = $log_box;
        
        return response()->json($response_data);
        
    }
    
    public static function getByBoxId(Request $request) {
        
        $box = Box::where('id', $request->box_id)->first();
        
        $response_data = self::getResponseData();
        $response_data->data = [
            "box" => $box,
            "log" => $box->log
        ];
        
        return response()->json($response_data);
        
    }
    
}