<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Box;

interface iSGAPIController {

    public static function create(Request $request);
    public static function update(Request $request);
    public static function get(Request $request);

}

class SGAPIController extends Controller implements iSGAPIController
{

    public static function test() {
        
        $boxes = Box::all();
        dd($boxes);
        
        //$farm = \App\Models\Farm::with('farm_box')->first();
        //$farm->box;
        //$farm_box = \App\Models\FarmBox::first();

        $box_logs = \App\Models\Log\LogBox::where('temperature', '>', 0)
                ->where('relative_humidity', '>', 0)
                ->get();

        foreach($box_logs as $box_log) {

            $new_box = new \App\Models\Log\LogBox($box_log->toArray());

            if($new_box->temperature > 0 || $new_box->relative_humidity > 0) {

                $new_box->temperature += self::rand(0, 3);
                $new_box->temperature = number_format($new_box->temperature, 2, '.', '');

                $new_box->relative_humidity += self::rand(0, 4);
                $new_box->relative_humidity = number_format($new_box->relative_humidity, 2, '.', '');

                $new_box->box_id = 9;

                //echo $box_log->temperature.' '.$new_box->temperature."<br>";
                //echo $box_log->relative_humidity.' '.$new_box->relative_humidity."<br>";

                $new_box->save();
            } else {
                echo 'not save';
            }

        }

        return 'test';
    }

    public static function create(Request $request) {

        $existed_box = Box::where('name', $request->name)->first();

        $response_data = self::getResponseData();


        if(is_null($existed_box)) {

            $box = new Box($request->toArray());
            $box->save();

            $response_data->data = [
                "box" => $box
            ];

            $response_data->message = 'Create Box id: '.$box->id;

        } else {
            $response_data->message = 'Box Name: '.$request->name.' is existed';
        }

        return response()->json($response_data);

    }

    public static function update(Request $request) {

        //dd(json_decode($request->data));
        $data = json_decode($request->data);
        
        $response_data = self::getResponseData();
        $response_data->data = [];
        //$response_data->box = new \Illuminate\Support\Collection();
        
        foreach($data as $box_data) {

            $box = Box::where('name', $box_data->name)->first();
            $log_box = [];
            
            if(!is_null($box)) {
                
                $box->fill((array) $box_data);                
                $box->save();

                $log_box = new \App\Models\Log\LogBox($box->toArray());
                $log_box->box_id = $box->id;
                $log_box->save();
                
                $message = 'updated';
                
            } else {
                $message = 'Box Name: '.$box_data->name.' is not existed';
            }
            
            $box_response = [
                "message" => $message,
                "box" => $box,                
                "log" => $log_box
            ];
            
            array_push($response_data->data , $box_response);
            
        }
        
        return response()->json($response_data);

    }

    public static function get(Request $request) {

        $box = Box::where('id', $request->id)->first();
        $logs = $box->log;

        $response_data = self::getResponseData();
        $response_data->data = [
            "box" => $box,
            "log" => $logs
        ];

        return response()->json($response_data);

    }

    public static function rand($min = 0, $max = 1)
    {
        return ($min + ($max - $min) * (mt_rand() / mt_getrandmax()));
    }

}
