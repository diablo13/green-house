<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Controller\API\CallController;
use App\Models\Datas\IPPBXCallData;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Faker\Provider\Uuid;

interface iCallController {
    public static function call();
}

class HrCall extends BaseController implements iCallController
{
    public static function call(){
   
        $station_id = Input::get('station_id', null);
        $phone = Input::get('phone', null);
        $call_id = \Faker\Provider\Uuid::uuid();
        $raw_data = [
            "message_code"=> "003",
            "agent_code"=> "admin_native",
            "station_id"=> $station_id,//8000-8001
            "mobile_phone"=> $phone,
            "datetime_response"=> Carbon::today()->toDateTimeString(),
            "call_id"=>  $call_id,
            "start_time"=> null,
            "end_time"=> null,
            "duration"=> null,
            "ringtime"=> null,
            "link_down_record"=> null,
            "status"=> null,
            "error_code"=> "0",
            "error_desc"=> ""
        ];
        $call_data = new IPPBXCallData($raw_data);
        $is_called = CallController::callVOIP($call_data);      
        $data = [
            'call_data' =>   $call_data ,
            'is_called' => json_decode($is_called)  
        ];

        return response()->json($data);
    }

}
