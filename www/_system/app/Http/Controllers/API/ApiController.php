<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Controller\API\CallController;
use App\Models\Datas\IPPBXCallData;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

use App\Http\Controllers\Controller;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Faker\Provider\Uuid;

interface iApiController {
}

class ApiController extends Controller implements iApiController
{
    
    public function test() {
        
        $data = \App\Models\View1Hour::where('mtimestamp', 'LIKE', '%2019-04-03%')->get();
        
        //dd($data);
        
        //return 'done';
        
        //$data = \Illuminate\Support\Facades\DB::connection('mysql2')->select('box');
        
        //dd(\App\Models\Box::all());
        
        //$line_data = json_decode($request->getContent())->originalDetectIntentRequest->payload->data;
        //$this->saveLog($line_data);
        
        //return 'done';
        
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient('<TIEuh8HJ9oLJAgHdJdbyynEKEi6Mn+cRA8de+iHCG9uMsPfFqssSwWA9MWyBIhB0Lpf/EtAUlyhtIi1/79wp6rRZ01Unc0SRZAJq4YR9Lx0ZSYWQpXOpkGHMadkic+2sC84UzZ2igvYZZjXlKXz/kgdB04t89/1O/w1cDnyilFU=');
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => '4a88efc9aac6021b755594db77e1956f']);
        
        //dd($httpClient, $bot);
        
        //$response = $bot->replyText('900c7223361c475b8e4edffdd593a650', 'hello!');
        
        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder('hello');
        $response = $bot->replyMessage('55f919eaee054204a318686485dd6ab8', $textMessageBuilder);
        if ($response->isSucceeded()) {
            echo 'Succeeded!';
            return;
        }

        // Failed
        echo $response->getHTTPStatus() . ' ' . $response->getRawBody();
        
        //dd($response);
        
        //return 'xxx';      
        
    }
    
    public function webhook(Request $request) {
        
        //save log
        $line_data = json_decode($request->getContent())->originalDetectIntentRequest->payload->data;
        $this->saveLog($line_data);
        
        $return_text = $this->getReturnText($line_data);
        
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient('<TIEuh8HJ9oLJAgHdJdbyynEKEi6Mn+cRA8de+iHCG9uMsPfFqssSwWA9MWyBIhB0Lpf/EtAUlyhtIi1/79wp6rRZ01Unc0SRZAJq4YR9Lx0ZSYWQpXOpkGHMadkic+2sC84UzZ2igvYZZjXlKXz/kgdB04t89/1O/w1cDnyilFU=');
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => '4a88efc9aac6021b755594db77e1956f']);
        
        //dd($httpClient, $bot);
        
        //$response = $bot->replyText('900c7223361c475b8e4edffdd593a650', 'hello!');
        
        //$return_text = "Hello\nasdasd\nasdasdasd";
        
        //$return_text = "xxx\nxx";
        
        //dd($return_text);
        
        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($return_text);
        //$textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($return_text);
        $response = $bot->replyMessage($line_data->replyToken, $textMessageBuilder);
        if ($response->isSucceeded()) {
            //echo 'Succeeded!';
            //return 'yes';
        }

        // Failed
        //echo $response->getHTTPStatus() . ' ' . $response->getRawBody();
        
        $line_msg = new \App\Models\LineMsg([
            'name' => 'test',
            'text' => $return_text,
            //'response' => $response, //json_encode($response),
        ]);
        
        $line_msg->save();
        
        return $return_text;
        
    }
    
    private function getReturnText($line_data) {
        
        $text = $line_data->message->text;
        $data = explode(" ", $line_data->message->text);
        
        //dd(count($data));
        
        $command = count($data) > 0 ? $data[0] : '';
        $type = count($data) > 1 ? $data[1] : '';
        $date = count($data) > 2 ? $data[2] : '';
       
        $return_str = "";
        
        if ($command == 'now' || $command == 'current') {
            return $this->getCurrent();                     
        } else if($command == 'service') {            
            //return = 'xx';
            $return_str .= "กรุณาพิมพ์คำสั่งด้านล่าง\n";
            $return_str .= "now - สำหรับดูค่าปัจจุบัน\n";
            $return_str .= "คำสั่งเจาะจงมากขึ้น ใช้รูปแบบ [คำสั่ง จำแนก วัน เวลา]\n";
            //$return_str .= "id - id โรงเรือน\n";
            $return_str .= "คำสั่ง - avg\n";
            //$return_str .= "จำแนก - ., rh, t\n";
            $return_str .= "วัน - dd, dd-dd, dd/mm, dd/mm-dd/mm, dd/mm/yyyy, dd/mm/yyyy-dd/mm/yyyy\n";
            //$return_str .= 'เวลา 0 ถึง 23                                        ';
            //$return_str .= 'ช่วงวัน yyyy/mm/dd-yyyy/mm/dd                             ';
            //$return_str .= 'ช่วงเวลา [0-23]-[0-23]                                       ';
            //$return_str .= 'ช่วงวันเวลา yyyy/mm/dd [0-23]-yyyy/mm/dd [0-23]                   ';
        } else if($command == 'avg' || $command == 'list'){
            return $this->getData($data);  
        } else if($command == 'dnote') {            
            $return_str .= "MORE FEATURE\n";
            $return_str .= "**** Deploy update service\n";
            $return_str .= "*** list minute-minute หาช่วงเวลา 30 บรรทัด\n";
            $return_str .= "** Notify springle time\n";
            $return_str .= "** Notify temp / rh SHOCK\n";
            $return_str .= "* create graph picture to send\n";            
            $return_str .= "avg dd t-t\n";
            $return_str .= "avg dd:t-dd:t\n";
            $return_str .= "avg box\n";            
        } else {
            $return_str = 'N/A';
        }
        
        return $return_str;
    }
    
    private function getCurrent() {
        
        $return_str = "".Carbon::now()."\n";
        
        $boxes = \App\Models\Box::where('id', '>=', 20)
                    ->get();            
        foreach ($boxes as $box) {
            $return_str .= "Box:".$box->id." Temperature: ".$box->temperature."c RH ".$box->relative_humidity."%\n";
        }
        
        $avg_temperature = number_format($boxes->avg('temperature'), 2, '.', '');
        $avg_rh = number_format($boxes->avg('relative_humidity'), 2, '.', '');
        
        $return_str .= 'AVG Temperature '.$avg_temperature.' AVG RH '.$avg_rh.'%';
        
        //return "\n".Carbon::now()."\n";
        
        return $return_str;   
    }
    
    private function getData($data) {
        
        $command = count($data) > 0 ? $data[0] : null;
        /*
        $type = count($data) > 1 ? $data[1] : null;
        $date = count($data) > 2 ? $data[2] : null;
        $time = count($data) > 3 ? $this->getValidateTime($data[3]) : null;
        */
        $date = count($data) > 1 ? $data[1] : null;
        $time = count($data) > 2 ? $data[2] : null;
        
        if(!is_null($date)) {
            
            $date_array = explode("-", $date);
            
            if(count($date_array) == 1) {
                
                $date = $this->normalizeDate($date_array[0]);    
                
                $time_str = "";
                
                if(is_null($time)) {
                    $log = \App\Models\View1Hour::where('mtimestamp', 'LIKE', '%'.$date.'%')->get();                    
                } else {                             
                    $log = \App\Models\View1Hour::where('mtimestamp', 'LIKE', '%'.$date.' '.$time.'%')->get();
                    $time_str .= " ".$time.":00:00";
                }
                
                $return_str = "".$this->denormalizeDate($date).$time_str;
                
                //dd('%'.str_replace('-', '/', $date).'%');
                
            } else {
                
                $from_date = $this->normalizeDate($date_array[0]);
                $to_date = $this->normalizeDate($date_array[1]);                
                $log = \App\Models\View1Hour::where('mtimestamp', '>=', $from_date)
                        ->where('mtimestamp', '<', $to_date)
                        ->get();
                
                $return_str = "".$this->denormalizeDate($from_date)."-".$this->denormalizeDate($to_date);
                
            }
            
            $return_str .= "\n";
            
            if($command == 'avg') {
            
                $avg_temperature = number_format($log->avg('avg_temperature'), 2, '.', '');
                $avg_rh = number_format($log->avg('avg_rh'), 2, '.', '');

                /*
                if($type == 'all' || $type == '.') {
                    $return_str .= "AVG Temperature\n ".$avg_temperature." AVG RH ".$avg_rh."%\n";
                } else if($type == 't') {
                    $return_str .= "AVG Temperature\n";
                } else if($type == 'rh') {
                    $return_str .= "AVG RH ".$avg_rh."%\n";
                }*/

                $return_str .= "AVG Temperature ".$avg_temperature."\nAVG RH ".$avg_rh."%\n";
                
            } else if($command == 'list') {
                
                $list_id = 0;
                $max_list = 24;
                
                $return_str .= "max list = ".$max_list."\n";
                
                foreach ($log as $log_data) {
                    
                    if($list_id >= $max_list) break;
                    
                    $date = Carbon::parse($log_data->mtimestamp)->format("d-m-Y H:i:s");
                    
                    $return_str .= $date." Temp ".number_format($log_data->avg_temperature, 2, '.', '')." RH ".number_format($log_data->avg_rh, 2, '.', '')."%\n";
                    $list_id++;
                }
                
            }

            //dd($return_str, $log, '%'.$date.'%','%'.$date.' '.$time.'%');

            return $return_str;
            
        } else {
            
            if(is_null($type) || ($type != '.' || $type != 't' || $type != 'rh')) {
                return 'Incorrect type';
            }
            
        }
    }
    
    private function saveLog($line_data) {
        
        $log_line_msg = new \App\Models\Log\LogLineMsg([
            'reply_token' => $line_data->replyToken,
            'user_id' => $line_data->source->userId,
            'text' => $line_data->message->text,
            'sent_at' => $line_data->timestamp,
            'created_at' => Carbon::now()
        ]);
        
        $log_line_msg->save();
        
    }
    
    //utility
    private function normalizeDate($date_str) {
        
        $data = explode("/", $date_str);
        $day = count($data) > 0 ? $data[0] : null;
        $month = count($data) > 1 ? $data[1] : null;
        $year = count($data) > 2 ? $data[2] : null;
        
        if(count($data) < 2) {
            $now = Carbon::now();
            $year = $now->year;
            $month = $now->month;            
        } else if(count($data) < 3) {
            $now = Carbon::now();
            $year = $now->year;
        }
        
        $date = Carbon::createMidnightDate($year, $month, $day);
        
        return $date->format('Y-m-d');
        
    }
    
    private function denormalizeDate($date_str) {
        
        $data = explode("-", $date_str);
        $day = $data[2];
        $month = $data[1];
        $year = $data[0];
        
        $date = Carbon::createMidnightDate($year, $month, $day);
        
        return $date->format('d/m/Y');
        
    }
    
}

?>
