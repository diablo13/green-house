<?php 

namespace App\Models\Log;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class LogBox extends Model
{
    
    use SoftDeletes;

    protected $table = 'log_box';

    protected $fillable = [
        'box_id',
        'temperature',
        'relative_humidity',
        'sent_at',
        'created_at', 'updated_at', 
    ];

    protected $hidden = [
        'deleted_at',
    ];
    
    public function box()
    {
        return $this->belongsTo('App\Models\Box');
    }
    
}
?>