<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    
    use SoftDeletes;
    
    protected $table = 'log';
    
    protected $fillable = [
        'action', 'object', 'user_id'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
}
