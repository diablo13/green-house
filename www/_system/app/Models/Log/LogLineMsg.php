<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\Model;

class LogLineMsg extends Model
{

    public $timestamps = false;
    //use SoftDeletes;
    
    protected $table = 'log_line_msg';
    
    protected $fillable = [
        'reply_token', 'user_id', 'text', 'sent_at', 'created_at',
    ];
    
    protected $hidden = [
    ];
    
}
