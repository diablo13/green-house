<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class LineMsg extends Model
{

    use SoftDeletes;
    
    protected $table = 'line_msg';
    
    protected $fillable = [
        'name',
        'text',
        'response',
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
}
