<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{

    use SoftDeletes;
    
    protected $connection = 'mysql2';
    protected $table = 'box';
    
    protected $fillable = [
        'name',
        'description',
        'temperature',
        'relative_humidity',
        'last_sent_at'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
    public function log()
    {
        return $this->hasMany('App\Models\Log\LogBox', 'box_id');
    }
    
}
